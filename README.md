# DEPRECATED

**As i'm unable to get it working with latest upstream changes in golang/signal, this project is deprecated. New project based on signal-cli (Java) can be found [here](https://gitlab.com/morph027/signal-cli-dbus-gateway/).**

## Documentation

* [Documentation](https://morph027.gitlab.io/signal-web-gateway)

## Support

If you like what i'm doing, you can support me via [Paypal](https://paypal.me/morph027), [Ko-Fi](https://ko-fi.com/morph027) or [Patreon](https://www.patreon.com/morph027).
