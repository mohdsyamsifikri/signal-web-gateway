+++
title = "Mail2Signal"
weight = 1
+++

## Mail-2-Signal Gateway

I've got a Hikvision network camera, which (only) can send email notifications and wanted to spice it up a little (that's why I started this whole gateway stuff).

So as we do have a signal web gateway running, let's just hook it up to a mail server.

### Setup

I use postfix with pipes and aliases. You might want to setup a dedicated instance like I did or re-use/extend your running relay/server. This assumes an already running postfix.

Now there's also a [dockerized version](https://gitlab.com/morph027/signal-web-gateway/tree/master/contrib/signal-mail-gateway).

#### Packages

To process the mails received, we need some packages:

* *mpack*: to unpack mime mails (attachements, text)
* *curl*: to trigger the web gateway

```bash
apt-get -y install mpack curl
```

#### Script

We do process mails with this script (just make sure to adjust the variables below `## init`):

```bash
#!/bin/bash

## init

# define signal-weg-gateway url
SIGNAL_GW="http://signal-gateway.example.com"
# define recipient
RECIPIENT="+123456789"

set -e

# create unique folder per mail
UUID=$(mktemp -d /tmp/XXXXXXXXXX)

# save mail
cat > $UUID/mail

# check if we received plain text message or multipart message
if grep multipart/mixed $UUID/mail; then
  # extract mail
  munpack -C $UUID $UUID/mail
  # get attachement if exists
  FILE=$(find $UUID \( -name '*.jpg' -o -name '*.png' \))
  # get text message if exists
  MESSAGE=$(find $UUID -name '*.desc')
else
  sed '1,/^$/d' $UUID/mail > $UUID/message
  MESSAGE=$UUID/message
fi

# trigger webhook
if [ ! "$MESSAGE" == "" ]; then
  REQUEST="curl -X POST -F to=$RECIPIENT -F message=$(cat $MESSAGE) $SIGNAL_GW"
  if [ ! "$FILE" == "" ]; then
    REQUEST="$REQUEST -F file=@$FILE"
  fi
  eval "$REQUEST"
fi

# delete mail
rm -rf "$UUID"
```

Place it somewhere reasonable like `/usr/local/bin/mail2signal.sh` and make it executable.

#### Postfix Alias

Add something like this to your `/etc/aliases` (do not forget to run `newaliases` after each change):

```
mail2signal: "|/usr/local/bin/mail2signal.sh"
```
