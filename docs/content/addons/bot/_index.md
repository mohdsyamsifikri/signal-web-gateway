+++
title = "Interactive Bot"
weight = 2
+++

## Signal Bot

Now as your monitoring solution is urging you with important messages, you probably want to respond and trigger certain actions or just fetch a cute animated cat to change your mood for the better.

The gateway binary incorporates a command line switch called `-hook` which obviously will trigger a hook for each message. As the original textsecure demo client was using fancy colors in interactive mode, there's also a switch called `-raw` to disable color codes within the message passed to the hook.

### Setup

{{% notice info %}}
This setup currently runs in standalone mode on systemd, as the gateway itself needs to read input data, which will be provided by a systemd socket.
{{% /notice %}}

Assuming you already have setup the gateway service according to the [instructions](/signal-web-gateway/installation/standalone/), we will extend the gateway service to trigger a hook on every message received.

#### Systemd

* add `-hook <any-hook-script-or-command> -raw` to `OPTS` in `/etc/default/signal-web-gateway`
* extend the gateway with a socket:

Example:

```
mkdir -p /etc/systemd/system/signal-web-gateway.service.d
cat > /etc/systemd/system/signal-web-gateway.service.d/socket.conf << EOF
[Service]
Sockets=signal-gateway.socket
StandardInput=socket
StandardOutput=journal
EOF
```

* create the socket:

Example:

```
cat > /etc/systemd/system/signal-web-gateway.socket << EOF
[Unit]
Description=Signal Gateway Socket

[Socket]
SocketUser=signal-gateway
Service=signal-gateway.service
ListenFIFO=/run/signal-web-gateway/socket
RemoveOnStop=on
EOF
```

* enable and start everything:

Example:

```
systemctl daemon-reload
systemctl enable signal-web-gateway.socket
systemctl restart signal-web-gateway.service
```
