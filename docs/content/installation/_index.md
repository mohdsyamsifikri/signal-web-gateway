+++
title = "Installation"
weight = 1
+++

* [Docker](/signal-web-gateway/installation/docker/)
* [Standalone](/signal-web-gateway/installation/standalone/)
