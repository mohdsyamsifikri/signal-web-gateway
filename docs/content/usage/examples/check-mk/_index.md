+++
title = "Check_MK"
+++

## Configuration

Create a file `/omd/sites/<site-name>/local/share/check_mk/notifications/signal-web-gateway`

```python
#!/usr/bin/python
# Signal Web Gateway
# Notification via Signal Web Gateway

import os, sys, requests


if os.environ['NOTIFY_WHAT'] == 'SERVICE':
    if os.environ['NOTIFY_SERVICESTATE'] == "OK":
        message = "\U00002705".decode("unicode-escape")
    elif os.environ['NOTIFY_SERVICESTATE'] == "WARNING":
        message = "\U000026A0".decode("unicode-escape")
    elif os.environ['NOTIFY_SERVICESTATE'] == "CRITICAL":
        message = "\U0000274C".decode("unicode-escape")
    elif os.environ['NOTIFY_SERVICESTATE'] == "UNKNOWN":
        message = "\U00002734".decode("unicode-escape")
    else:
        message = ""
    message += " " + os.environ['NOTIFY_HOSTNAME'] + " "
    message += os.environ['NOTIFY_SERVICESTATE'] + " "
    message += os.environ['NOTIFY_SERVICEDESC'] + " "
    message += os.environ['NOTIFY_SERVICEOUTPUT'].decode('utf-8') 
else:
    message += "is " + os.environ['NOTIFY_HOSTSTATE']

recipient = os.environ['NOTIFY_CONTACTPAGER']

if not recipient:
    sys.stderr.write('Error: No contact pager set for %s\n' % os.environ['NOTIFY_CONTACTNAME'])
    sys.exit(1)

requests.post(os.environ['NOTIFY_PARAMETER_1'], data={'message': message, 'to': recipient})
```

Now you can setup the notifications for your user or global. Create a new notification rule and select **Signal Web Gateway** as *Notification Method*. Enter the gateway URL as first parameter. Finally setup a *Pager address* in your users account to receive messages.
