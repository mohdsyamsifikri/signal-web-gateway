+++
title = "Home Assistant"
+++

## Configuration

Add a notify service to your `configuration.yaml`:

```yaml
notify:
  - name: signal
    platform: rest
    resource: https://signal-gateway.example.com/json/<recipient-or-group-id>
    method: POST_JSON
```

Now you can use it in automations like this:

```yaml
  - data:
      message: something happened
    service: notify.signal
```

![](/signal-web-gateway/img/hass.png)
