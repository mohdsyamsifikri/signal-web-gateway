#!/bin/bash

set -e

docker login -u gitlab-ci-token -p $CI_BUILD_TOKEN $CI_REGISTRY
apk --no-cache add curl
curl -Lo /usr/local/bin/buildx https://github.com/docker/buildx/releases/download/v${BUILDX_VERSION}/buildx-v${BUILDX_VERSION}.linux-amd64
chmod +x /usr/local/bin/buildx
buildx create --name multiarch
buildx use multiarch
buildx inspect --bootstrap
buildx="buildx build --push"
docker pull $CI_REGISTRY/$(echo $CI_PROJECT_NAMESPACE | tr '[:upper:]' '[:lower:]')/$CI_PROJECT_NAME:latest || /bin/true
${buildx} --platform ${PLATFORMS} \
  --build-arg version="${version}" \
  --cache-from $CI_REGISTRY/$(echo $CI_PROJECT_NAMESPACE | tr '[:upper:]' '[:lower:]')/$CI_PROJECT_NAME:latest \
  -t $CI_REGISTRY/$(echo $CI_PROJECT_NAMESPACE | tr '[:upper:]' '[:lower:]')/$CI_PROJECT_NAME:${CI_COMMIT_REF_SLUG}-${CI_COMMIT_SHORT_SHA} \
  -t $CI_REGISTRY/$(echo $CI_PROJECT_NAMESPACE | tr '[:upper:]' '[:lower:]')/$CI_PROJECT_NAME:latest \
  -f docker/Dockerfile docker
